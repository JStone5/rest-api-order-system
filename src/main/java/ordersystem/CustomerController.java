package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.support.SimpleTriggerContext;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

    private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
    List<Customer> customerArray = new ArrayList<>(10);

    /**
     * Create a new customer in the database
     *
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
        customer.setNumber(counter.incrementAndGet());
        customerDb.put(customer.getNumber(), customer);
        customerArray.add(customer);
        return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }

    /**
     * Get a customer from the database
     *
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
        if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin() // to allow CORS requests when running as a local server
    @PutMapping("/customers/{number}")
    public ResponseEntity<Object> putNameChange(@PathVariable long number, @RequestBody Customer customer) {
        if (customerDb.containsKey(number)) {
            customer.setFirstName(customer.firstName);
            customer.setLastName(customer.lastName);
            return new ResponseEntity<>("Customer successfully updated", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/customers/customerArray")
    public ResponseEntity<String> getCustomerArray(@PathVariable List<Customer> customerArray) {
        for (int i = 0; i < customerArray.size(); i++) {
            if (customerArray.isEmpty()) {
                return new ResponseEntity<>("Order does not exist", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(Arrays.toString(customerArray.toArray()), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(Arrays.toString(customerArray.toArray()), HttpStatus.OK);
    }

    @GetMapping("/customers/getNum")
    public ResponseEntity<Long> getCustomerNumber(@RequestBody Customer customer) {
            return new ResponseEntity<>(customer.getNumber(), HttpStatus.OK);
    }
}